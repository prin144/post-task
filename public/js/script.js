 // List of sentences
 var _CONTENT = [ "Genshin Map ", "Google Map ", "Post Task" ];
        
 // Current sentence being processed
 var _PART = 0;
 
 // Character number of the current sentence being processed 
 var _PART_INDEX = 0;
 
 // Holds the handle returned from setInterval
 var _INTERVAL_VAL;
 
 // Element that holds the text
 var _ELEMENT = document.querySelector(".dynamic");
 
 // Implements typing effect
 function Type() { 
     var text =  _CONTENT[_PART].substring(0, _PART_INDEX + 1);
     _ELEMENT.innerHTML = text;
     _PART_INDEX++;
 
     // If full sentence has been displayed then start to delete the sentence after some time
     if(text === _CONTENT[_PART]) {
         clearInterval(_INTERVAL_VAL);
         setTimeout(function() {
             _INTERVAL_VAL = setInterval(Delete, 50);
         }, 1000);
     }
 }
 
 // Implements deleting effect
 function Delete() {
     var text =  _CONTENT[_PART].substring(0, _PART_INDEX - 1);
     _ELEMENT.innerHTML = text;
     _PART_INDEX--;
 
     // If sentence has been deleted then start to display the next sentence
     if(text === '') {
         clearInterval(_INTERVAL_VAL);
 
         // If last sentence then display the first one, else move to the next
         if(_PART == (_CONTENT.length - 1))
             _PART = 0;
         else
             _PART++;
         _PART_INDEX = 0;
 
         // Start to display the next sentence after some time
         setTimeout(function() {
             _INTERVAL_VAL = setInterval(Type, 100);
         }, 200);
     }
 }
 
 // Start the typing effect on load
 _INTERVAL_VAL = setInterval(Type, 100);











// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
let map, infoWindow;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: {lat: 14.680244922011696, lng: 120.93992726013157},
    zoom: 8,
    mapId: 'd431710971253659'
  });

  const statuemnl = new google.maps.Marker({
    position: {lat: 14.599642890154122, lng: 120.98237278710731},
    map: map,
    title: "Manila",
    icon: {
        url: "img/Statue.svg",
        scaledSize: new google.maps.Size(48, 41)
    }
  });

  const statueceb = new google.maps.Marker({
    position: {lat: 10.378756863916214, lng: 123.85916656731833},
    map: map,
    title: "Cebu",
    icon: {
        url: "img/Statue.svg",
        scaledSize: new google.maps.Size(48, 41)
    }
  });

  const statuedav = new google.maps.Marker({
    position: {lat: 7.299594754531105, lng: 125.4036489961714},
    map: map,
    title: "Davao",
    icon: {
        url: "img/Statue.svg",
        scaledSize: new google.maps.Size(48, 41)
    }
  });

  const wp1 = new google.maps.Marker({
    position: {lat: 17.530274958979952, lng: 121.23452834518551},
    map: map,
    icon: {
        url: "img/waypoint.svg",
        scaledSize: new google.maps.Size(43, 36)
    }
  });
  
  const wp2 = new google.maps.Marker({
    position: {lat: 12.130415674567159, lng: 125.00856541114524},
    map: map,
    icon: {
        url: "img/waypoint.svg",
        scaledSize: new google.maps.Size(43, 36)
    }
  });
  
  const wp3 = new google.maps.Marker({
    position: {lat: 10.114673304060238, lng: 119.01003065693453},
    map: map,
    icon: {
        url: "img/waypoint.svg",
        scaledSize: new google.maps.Size(43, 36)
    }
  });

  infoWindow = new google.maps.InfoWindow();

  const locationButton = document.createElement("button");

  locationButton.textContent = "Pan to Current Location";
  locationButton.classList.add("custom-map-control-button");
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
  locationButton.addEventListener("click", () => {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };

          const player = new google.maps.Marker({
            position: pos,
            map: map,
            title: "You are Here",
            icon: {
                url: "img/player_pin.svg",
                scaledSize: new google.maps.Size(38, 31)
            }
          });

          infoWindow.open(map);
          map.setCenter(pos);
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
  });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}